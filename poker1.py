from itertools import combinations
from collections import defaultdict
rank_value_dict = {"2":2, "3":3, "4":4, "5":5, "6":6, "7":7, "8":8, "9":9, "T":10,"J":11, "Q":12, "K":13, "A":14}
def number_of_players():
	while True:
		players = int(input("Enter the number of players (1 to 8): "))
		if (players>=1 and players<=8):
			return(players)
def card_checker(card):
	suits=['C','D','H','S']
	ranks=['2','3','4','5','6','7','8','9','T','J','Q','K','A']
	if not len(card)==2:
		return False
	if card[-1] in suits:
		if card[0] in ranks:
			return True
	else:
		return False
def input_hole_cards(players):
	hole_cards=[]
	player_hole_cards=[]
	for i in range(0,players):
		player_hole_cards.append([])
		print("\nPlayer "+str(i+1)+": ")
		for j in range(1,3):
			check=True	
			while check:
				input_card = input("Enter card "+str(j)+ ": ").upper()
				if not input_card in hole_cards and card_checker(input_card):
					player_hole_cards[i].append(input_card)
					hole_cards.append(input_card)
					check=False
				else:
					print("Invalid card entered")
	return [player_hole_cards,hole_cards]
def community_card_combinations(hole_cards):
	community_cards=[]
	suits=['C','D','H','S']
	ranks=['2','3','4','5','6','7','8','9','T','J','Q','K','A']
	for i in suits:
		for j in ranks:
			community_cards.append(j+i)
	for i in hole_cards:
		if i in community_cards:
			community_cards.remove(i)
	community_card_combinations_list=list(combinations(community_cards,5))
	for i in range(0,len(community_card_combinations_list)):
		community_card_combinations_list[i]=list(community_card_combinations_list[i])
	return community_card_combinations_list
def seven_card_combinations_generator(player_hole_cards, community_card_combinations):
	seven_card_combinations=[]
	for e, i in enumerate(community_card_combinations):
		seven_card_combinations.append([])
		for j in player_hole_cards:
			seven_card_combinations[e].append(j+i)
	return seven_card_combinations
def hand_combinations(seven_card_combination):
	hand_combinations_list=list(combinations(seven_card_combination, 5))
	for i in range(0,len(hand_combinations_list)):
		hand_combinations_list[i]=list(hand_combinations_list[i])
	return hand_combinations_list
def hand_combinations_evaluator(hand_combinations_list):
	highest_valuation=[0]
	highest_hand=None
	for hand in hand_combinations_list:
		current_valuation=check_hand(hand)
		if current_valuation[0]>highest_valuation[0]:
			highest_valuation=current_valuation
			highest_hand=hand
		if current_valuation[0]==highest_valuation[0]:
			for i in range(1,len(current_valuation)):
				if current_valuation[i]>highest_valuation[i]:
					highest_valuation=current_valuation
					highest_hand=hand
				elif current_valuation[i]<highest_valuation[i]:
					break
	return [highest_hand,highest_valuation]
def valuation_comparison(valuation_list):
	highest_valuation=[0]
	winner=None
	draw_counter=0
	for i in range(0,len(valuation_list)):
		if valuation_list[i]==highest_valuation:
			draw_counter+=1
		elif int(valuation_list[i][0])>int(highest_valuation[0]):
			highest_valuation=valuation_list[i]
			winner=i+1
		elif int(valuation_list[i][0])==int(highest_valuation[0]):
			for j in range(1,len(valuation_list[i])):
				if valuation_list[i][j]>highest_valuation[j]:
					highest_valuation=valuation_list[i]
					winner=i+1
				elif valuation_list[i][j]<highest_valuation[i]:
					break
		if (draw_counter+1)==len(valuation_list):
			return -1
	return winner

def check_four_of_a_kind(hand):
	ranks=[h[0] for h in hand]
	value_count_dict=defaultdict(lambda:0)
	for r in ranks:
		value_count_dict[r]+=1
	if sorted(value_count_dict.values())==[1,4]:
		for r in ranks:
			if value_count_dict[r]==4:
				quadruplet=rank_value_dict[r]
			else:
				singleton=rank_value_dict[r]
		return [True,8,quadruplet,singleton]
	return [False]
def check_full_house(hand):
	ranks=[h[0] for h in hand]
	value_count_dict=defaultdict(lambda:0)
	for r in ranks:
		value_count_dict[r]+=1
	if sorted(value_count_dict.values())==[2,3]:
		for r in ranks:
			if value_count_dict[r]==3:
				triplet=rank_value_dict[r]
			elif value_count_dict[r]==2:
				pair=rank_value_dict[r]
		return [True,7,triplet,pair]
	return [False]
def check_flush(hand):
	suits=[h[1] for h in hand]
	if len(set(suits))==1:
		ranks=[h[0] for h in hand]
		rank_list=sorted([rank_value_dict[r] for r in ranks],reverse=True)
		return ([True,6]+rank_list)
	return [False]
def check_straight(hand):
	ranks=[h[0] for h in hand]
	value_count_dict=defaultdict(lambda:0)
	for r in ranks:
		value_count_dict[r]+=1
	rank_value_list=[rank_value_dict[r] for r in ranks]
	if (len(set(value_count_dict.values()))==1) and (max(rank_value_list)-min(rank_value_list))==4:
		return [True, 5, (max(rank_value_list))]
	elif set(ranks)==set(["A","2","3","4","5"]):
			return [True,5,5]
	return [False]	
def check_three_of_a_kind(hand):
	ranks=[h[0] for h in hand]
	value_count_dict=defaultdict(lambda:0)
	for r in ranks:
		value_count_dict[r]+=1
	if set(value_count_dict.values())==set([3,1]):
		for r in ranks:
			if value_count_dict[r]==3:
				triplet=rank_value_dict[r]
		ranks[:]=(i for i in ranks if rank_value_dict[i]!=triplet)
		rank_list=sorted([rank_value_dict[r] for r in ranks],reverse=True)
		return ([True,4]+[triplet]+rank_list)
	return [False]
def check_two_pair(hand):
	ranks=[h[0] for h in hand]
	value_count_dict=defaultdict(lambda:0)
	for r in ranks:
		value_count_dict[r]+=1
	if sorted(value_count_dict.values())==[1,2,2]:
		pairs=[]
		for r in ranks:
			if value_count_dict[r]==2:
				pairs.append(rank_value_dict[r])
		pairs=sorted(list(set(pairs)), reverse=True)
		ranks[:]=(i for i in ranks if not (rank_value_dict[i] in pairs))
		rank_list=sorted([rank_value_dict[r] for r in ranks],reverse=True)
		return ([True,3]+pairs+rank_list)
	return [False]
def check_pair(hand):
	ranks=[h[0] for h in hand]
	value_count_dict=defaultdict(lambda:0)
	for r in ranks:
		value_count_dict[r]+=1
	if sorted(value_count_dict.values())==[1,1,1,2]:
		for r in ranks:
			if value_count_dict[r]==2:
				pair=rank_value_dict[r]
		ranks[:]=(i for i in ranks if rank_value_dict[i]!=pair)
		rank_list=sorted([rank_value_dict[r] for r in ranks],reverse=True)
		return ([True,2]+[pair]+rank_list)
	return [False]
def check_high_card(hand):
	ranks=[h[0] for h in hand]
	rank_list=sorted([rank_value_dict[r] for r in ranks],reverse=True)
	return ([True,1]+rank_list)
def check_straight_flush(hand):
	straight=check_straight(hand)
	flush=check_flush(hand)
	if straight[0] and flush[0]:
		return [True,9,straight[-1]]
	return [False] 
def check_hand(hand):
	check=check_straight_flush(hand)
	if check[0]==True:
		del check[0]
		return check
	check=check_four_of_a_kind(hand)
	if check[0]==True:
		del check[0]
		return check
	check=check_full_house(hand)
	if check[0]==True:
		del check[0]
		return check
	check=check_flush(hand)
	if check[0]==True:
		del check[0]
		return check
	check=check_straight(hand)
	if check[0]==True:
		del check[0]
		return check
	check=check_three_of_a_kind(hand)
	if check[0]==True:
		del check[0]
		return check
	check=check_two_pair(hand)
	if check[0]==True:
		del check[0]
		return check
	check=check_pair(hand)
	if check[0]==True:
		del check[0]
		return check
	check=check_high_card(hand)
	if check[0]==True:
		del check[0]
		return check


number_of_players=number_of_players()
hole_cards=input_hole_cards(number_of_players)
player_hole_cards=hole_cards[0]
hole_cards=hole_cards[1]
community_card_combinations=community_card_combinations(hole_cards)
seven_card_combinations=seven_card_combinations_generator(player_hole_cards, community_card_combinations)
win_counter=[0]*number_of_players
ctr=0
draws=0
for i in seven_card_combinations:
	ctr=ctr+1
	print("\n"+str(ctr))
	final_evaluation_list=[]
	for p,j in enumerate(i):
		best_hand=(hand_combinations_evaluator(hand_combinations(j)))
		final_evaluation_list.append(best_hand[1])
		print("Player "+str(p+1)+": "+str(j)+"   Best Hand: "+str(best_hand[0]))
	print(final_evaluation_list)
	winner=valuation_comparison(final_evaluation_list)
	if winner ==-1:
		print("Draw!")
		draws+=1
	else:
		win_counter[winner-1]+=1
		print("Result: Player "+str(winner)+" Wins!")
print(win_counter)
print(draws)
