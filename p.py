from itertools import combinations
from collections import defaultdict

suit_value_dict={"C":0,"D":1,"H":2,"S":3}
rank_value_dict={"A":41,"K":37,"Q":31,"J":29,"T":23,"9":19,"8":17,"7":13,"6":11,"5":7,"4":5,"3":3,"2":2}
rank_list=["A","K","Q","J","T","9","8","7","6","5","4","3","2"]
deck_list=[rank+suit for rank in rank_value_dict for suit in suit_value_dict]

straight_flushes_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+1) for e,i in enumerate([(str(rank_list[x])+str(rank_list[x+1])+str(rank_list[x+2])+str(rank_list[x+3])+str(rank_list[x+4])) for x in range(0,9)]+["5432A"])}
fours_of_a_kind_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+11) for e,i in enumerate([str(x)+str(x)+str(x)+str(x)+str(y) for x in rank_value_dict for y in rank_value_dict if not x==y])}
full_houses_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+167) for e,i in enumerate([str(x)+str(x)+str(x)+str(y)+str(y) for x in rank_value_dict for y in rank_value_dict if not x==y])}
flushes_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+323) for e,i in enumerate([x[0]+x[1]+x[2]+x[3]+x[4] for x in combinations("AKQJT98765432",5) if not (rank_value_dict[str(x[0])]*rank_value_dict[str(x[1])]*rank_value_dict[str(x[2])]*rank_value_dict[str(x[3])]*rank_value_dict[str(x[4])]) in straight_flushes_value_dict])}
straights_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+1600) for e,i in enumerate([(str(rank_list[x])+str(rank_list[x+1])+str(rank_list[x+2])+str(rank_list[x+3])+str(rank_list[x+4])) for x in range(0,9)]+["5432A"])}
threes_of_a_kind_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+1610) for e,i in enumerate([str(rank_list[x])+str(rank_list[x])+str(rank_list[x])+str(rank_list[y])+str(rank_list[z]) for x in range(0,13) for y in range(0,13) for z in range(y+1,13) if not x==y and not x==z and not y==z])}
two_pairs_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+2468) for e,i in enumerate([str(rank_list[x])+str(rank_list[x])+str(rank_list[y])+str(rank_list[y])+str(rank_list[z]) for x in range(0,13) for y in range(x+1,13) for z in range(0,13) if not x==y and not x==z and not y==z])}
one_pair_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+3326) for e,i in enumerate([str(rank_list[w])+str(rank_list[w])+str(rank_list[x])+str(rank_list[y])+str(rank_list[z]) for w in range(0,13) for x in range(0,13) for y in range(x+1,13) for z in range(y+1,13) if not w==x and not w==y and not w==z and not x==y and not x==z and not y==z])}
high_card_value_dict={(rank_value_dict[str(i[0])]*rank_value_dict[str(i[1])]*rank_value_dict[str(i[2])]*rank_value_dict[str(i[3])]*rank_value_dict[str(i[4])]):(e+6186) for e,i in enumerate([x[0]+x[1]+x[2]+x[3]+x[4] for x in combinations("AKQJT98765432",5) if not (rank_value_dict[str(x[0])]*rank_value_dict[str(x[1])]*rank_value_dict[str(x[2])]*rank_value_dict[str(x[3])]*rank_value_dict[str(x[4])]) in straight_flushes_value_dict])}


def Input_Number_Of_Players():
	while True:
		number_of_players = int(input("Enter the number of players (2 to 8): "))
		if number_of_players in range(2,9):
			return number_of_players
def Input_Starting_Cards(number_of_players): # Returns starting_cards_list
	starting_cards_list=[]
	for player in range(0,number_of_players):
		starting_cards_list.append([])
		card_number=0
		print("\nPlayer",(player+1))
		while card_number<2:
			card=(input("Card "+str(card_number+1)+": ")).upper()
			if len(card)==2:
				rank=card[0]
				suit=card[1]
				if card in deck_list:
					starting_cards_list[player].append(card)
					deck_list.remove(card)
					card_number+=1
	return starting_cards_list
def Community_Cards_Combinations():
	community_cards_combinations_list=[list(x) for x in combinations(deck_list,5)]
	return community_cards_combinations_list
def Possible_Games(starting_cards_list, community_cards_combinations_list): # Returns possible_games_list
	possible_games_list=[]
	for i,x in enumerate(community_cards_combinations_list):
		possible_games_list.append([])
		for j,y in enumerate(starting_cards_list):
			possible_hands_list=[a[0]+a[1]+a[2]+a[3]+a[4] for a in [list(b) for b in combinations(y+x,5)]]
			possible_games_list[i].append(Best_Hand(possible_hands_list))
	return possible_games_list
def Best_Hand(hand_list):
	highest_hand=None
	highest_hand_value=9999
	for hand in hand_list:
		hand_value=Hand_Evaluator(hand)
		if hand_value<highest_hand_value:
			highest_hand=hand
			highest_hand_value=hand_value
	return highest_hand
def Hand_Evaluator(hand):
	suits=[hand[1],hand[3],hand[5],hand[7],hand[9]]
	ranks=[hand[0],hand[2],hand[4],hand[6],hand[8]]
	prime_product=(rank_value_dict[str(ranks[0])]*rank_value_dict[str(ranks[1])]*rank_value_dict[str(ranks[2])]*rank_value_dict[str(ranks[3])]*rank_value_dict[str(ranks[4])])
	if len(set(suits))==1:
		if prime_product in straight_flushes_value_dict:
			return straight_flushes_value_dict[prime_product]
		return flushes_value_dict[prime_product]
	else:
		if prime_product in fours_of_a_kind_value_dict:
			return fours_of_a_kind_value_dict[prime_product]
		if prime_product in full_houses_value_dict:
			return full_houses_value_dict[prime_product]
		if prime_product in straights_value_dict:
			return straights_value_dict[prime_product]
		if prime_product in threes_of_a_kind_value_dict:
			return threes_of_a_kind_value_dict[prime_product]
		if prime_product in two_pairs_value_dict:
			return two_pairs_value_dict[prime_product]
		if prime_product in one_pair_value_dict:
			return one_pair_value_dict[prime_product]
		if prime_product in high_card_value_dict:
			return high_card_value_dict[prime_product]
def Game_Simulation(possible_games_list):
	game_simulation_results=[]
	for game in possible_games_list:
		winner=[]
		highest_hand_value=9999
		for i, player in enumerate(game):
			hand_value=Hand_Evaluator(player)
			if hand_value<highest_hand_value:
				winner=[i+1]
				highest_hand_value=hand_value
			elif hand_value==highest_hand_value:
				winner.append(i+1)
		game_simulation_results.append(winner)
	return game_simulation_results
def Game_Simulation_Results_Tabulator(game_simulation_results):
	result_tabulation=defaultdict(lambda:0)
	for result in game_simulation_results:
		result_tabulation[str(result)]+=1
	return result_tabulation
def main():
	number_of_players=Input_Number_Of_Players()
	starting_cards_list=Input_Starting_Cards(number_of_players)
	community_cards_combinations_list=Community_Cards_Combinations()
	possible_games_list=Possible_Games(starting_cards_list, community_cards_combinations_list)
	game_simulation_results=Game_Simulation(possible_games_list)
	result_tabulation=Game_Simulation_Results_Tabulator(game_simulation_results)
	print((result_tabulation))
main()